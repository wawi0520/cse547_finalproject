/*!
*      \file HarmonicMapper.h
*      \brief Algorithm for harmonic mapping
*	   \author David Gu
*      \date Document 10/07/2010
*
*		Simple harmonic map, that maps a topological disk to the unit disk
*		with minimal harmonic energy.
*/

// Simple harmonic map, that maps topological disk to a unit disk using harmonic map.

#ifndef _HARMONIC_MAPPER_H_
#define _HARMONIC_MAPPER_H_

#include "Mesh/iterators.h"
#include "Eigen/Sparse"
#include <vector>
#include "HarmonicMapperMesh.h"
#include <cmath>

#ifndef PI
#define PI 3.1415926535
#endif

namespace MeshLib
{
    // For normalization
    const double disturbance = 1;
    const double scalingFactor = 2;

	template<typename M>
	class CHarmonicMapper
	{
	public:
		/*!	CHarmonicMapper constructor
		 *	\param pMesh the input mesh
		 */
		CHarmonicMapper( M* pMesh);
		/*!	CHarmonicMapper destructor
		 */
		~CHarmonicMapper();
		/*!  Compute the harmonic map using direct method
		 */
		void _map();
		/*!	Iterative method compute harmonic map
		 *	\param epsilon error threshould
		 */
		void _iterative_map( double threshould = 1e-6 );

	protected:
		/*!	fix the boundary vertices to the unit circle
		 *  using arc length parameter
		 */
		void _set_boundary();
		/*!
		 *	compute edge weight
		 */
		void _calculate_edge_weight();

	protected:
		/*!	The input surface mesh
		 */
		M* m_pMesh;
		/*!	The boundary of m_pMesh
		 */
		typename M::CBoundary m_boundary;

		/*!	number of interior vertices
		 */
		int m_interior_vertices;
		/*! number of boundary vertices
		*/
		int m_boundary_vertices;
		/*! cosine law, return A
		 */
		double _inverse_cosine_law( double a, double b, double c );
	};

template<typename M>
double CHarmonicMapper<M>::_inverse_cosine_law( double a, double b, double c )
{
	 double cs =  ( a*a + b * b  - c * c )/( 2.0 * a * b );
     assert( cs <= 1.0 && cs >= -1.0 );
     return acos( cs );
}

template<typename M>
void CHarmonicMapper<M>::_calculate_edge_weight()
{
    //compute edge length
    for( M::MeshEdgeIterator eiter( m_pMesh ); !eiter.end(); ++eiter )
    {
        M::CEdge   *pE = *eiter;
        M::CVertex *pV1 = m_pMesh->edgeVertex1( pE );
        M::CVertex *pV2 = m_pMesh->edgeVertex2( pE );
        pE->length() = ( pV2->point() - pV1->point() ).norm();
    }

    //compute corner angle
    for( M::MeshFaceIterator filter( m_pMesh ); !filter.end(); ++filter )
    {
        M::CFace *pF = *filter;
        std::vector<M::CHalfEdge *> halfEdges;
        for( M::FaceHalfedgeIterator hiter( pF ); !hiter.end(); ++hiter )
        {
            halfEdges.push_back( *hiter );
        }

        // Make sure we have three half edge for each face.
        assert( 3 == halfEdges.size() );

        std::vector<double> lengths;
        for( int i = 0; i < halfEdges.size(); ++i )
        {
            // Iterate each half edge and retrieve the length.
            M::CEdge *pHe = m_pMesh->halfedgeEdge( halfEdges[i] );
            lengths.push_back( pHe->length() );
        }
        // Make sure we have three edge lengths for each face.
        assert( 3 == lengths.size() );

        // Compute the angle against the half edge
        for( int i = 0; i < lengths.size(); ++i )
        {
            halfEdges[i]->angle() = acos( (lengths[i]*lengths[i]+lengths[(i+1)%3]*lengths[(i+1)%3]-lengths[(i+2)%3]*lengths[(i+2)%3]) / (2*lengths[i]*lengths[(i+1)%3]));
        }
    }

    //compute edge weight
    for( M::MeshEdgeIterator eiter( m_pMesh ); !eiter.end();  ++eiter )
    {
        M::CEdge *pE = *eiter;
        M::CHalfEdge * pHe1 = m_pMesh->edgeHalfedge( pE, 0 );
        M::CHalfEdge *pHe2 = m_pMesh->halfedgeSym( pHe1 );

        double angle = m_pMesh->faceNextCcwHalfEdge( pHe1 )->angle();
        double weight = cos( angle ) / sin( angle );
        if( NULL != pHe2 )
        {
            angle = m_pMesh->faceNextCcwHalfEdge( pHe2 )->angle();
            weight += cos( angle ) / sin( angle );
        }

        pE->weight() = weight;
    }
}

/*!	CHarmonicMapper constructor
*	Count the number of interior vertices, boundary vertices and the edge weight
*
*/
template<typename M>
CHarmonicMapper<M>::CHarmonicMapper( M* pMesh ): m_pMesh( pMesh ), m_boundary( m_pMesh )
{

	int vid  = 0; //interior vertex ID
	int vfid = 0; //boundary vertex ID

	for( M::MeshVertexIterator viter( m_pMesh ); !viter.end(); ++ viter )
	{
		M::CVertex * pV = *viter;

		if( pV->boundary() )
		{
			pV->idx() = vfid ++;
		}
		else
		{
			pV->idx() = vid  ++;
		}
	}

	m_interior_vertices = vid;
	m_boundary_vertices = vfid;

	_calculate_edge_weight();

};

//Destructor
/*!
 *	CHarmonicMapper destructor
 */
template<typename M>
CHarmonicMapper<M>::~CHarmonicMapper()
{
};


//Set the boundary vertices to the unit circle
/*!
 *	Fix the boundary using arc length parameter
 */
template<typename M>
void CHarmonicMapper<M>::_set_boundary()
{
	//get the boundary half edge loop
	std::vector<M::CLoop*> & pLs =  m_boundary.loops();
	assert( pLs.size() == 1 );
	M::CLoop * pL = pLs[0];

	//compute the total length of the boundary
	const double totalLength = pL->length();

	//parameterize the boundary using arc length parameter
	double length = 0;
	std::list<M::CHalfEdge*> & pHs = pL->halfedges();
    std::list<M::CHalfEdge*>::iterator hiter = pHs.begin();
	for( ; hiter != pHs.end(); ++hiter )
	{
		M::CHalfEdge *pHe = *hiter;
		M::CEdge *pE = (M::CEdge *)(pHe->edge());
		length += pE->length();

        const double angle = length / totalLength * 2.0 * PI;
        const double px = ( disturbance + cos( angle ) ) / scalingFactor;
        const double py = ( disturbance + sin( angle ) ) / scalingFactor;
        const CPoint2 point( px, py );

		M::CVertex *pV = (M::CVertex *)(pHe->target());
		pV->huv() = point;
	}
}

//Compute the harmonic map with the boundary condition, direct method
/*!	Compute harmonic map using direct method
*/
template<typename M>
void CHarmonicMapper<M>::_map()
{

    //fix the boundary
    _set_boundary();

    std::vector<Eigen::Triplet<double> > A_coefficients;
    std::vector<Eigen::Triplet<double> > B_coefficients;


    //set the matrix A
    for( M::MeshVertexIterator viter( m_pMesh ); !viter.end(); ++ viter )
    {
        M::CVertex * pV = *viter;
        if( pV->boundary() ) continue;
        int vid = pV->idx();

        double sw = 0;
        for( M::VertexVertexIterator witer( pV ); !witer.end(); ++ witer )
        {
            M::CVertex * pW = *witer;
            int wid = pW->idx();

            M::CEdge * e = m_pMesh->vertexEdge( pV, pW );
            double w = e->weight();

            if( pW->boundary() )
            {
                B_coefficients.push_back( Eigen::Triplet<double>(vid,wid,w) );
            }
            else
            {
                A_coefficients.push_back( Eigen::Triplet<double>(vid,wid, -w) );
            }
            sw += w;
        }
        A_coefficients.push_back( Eigen::Triplet<double>(vid,vid, sw ) );
    }


    Eigen::SparseMatrix<double> A( m_interior_vertices, m_interior_vertices );
    A.setZero();

    Eigen::SparseMatrix<double> B( m_interior_vertices, m_boundary_vertices );
    B.setZero();
    A.setFromTriplets(A_coefficients.begin(), A_coefficients.end());
    B.setFromTriplets(B_coefficients.begin(), B_coefficients.end());


    Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
    std::cerr << "Eigen Decomposition" << std::endl;
    solver.compute(A);
    std::cerr << "Eigen Decomposition Finished" << std::endl;

    if( solver.info() != Eigen::Success )
    {
        std::cerr << "Waring: Eigen decomposition failed" << std::endl;
    }


    for( int k = 0; k < 2; k ++ )
    {
        Eigen::VectorXd b(m_boundary_vertices);
        //set boundary constraints b vector
        for( M::MeshVertexIterator viter( m_pMesh ); !viter.end(); ++viter )
        {
            M::CVertex * pV = *viter;
            if( pV->boundary() )
            {
                int id = pV->idx();
                b(id) = pV->huv()[k];
            }
        }
        Eigen::VectorXd c(m_interior_vertices);
        c = B * b;

        Eigen::VectorXd x = solver.solve(c);
        if( solver.info() != Eigen::Success )
        {
            std::cerr << "Waring: Eigen decomposition failed" << std::endl;
        }

        //set the images of the harmonic map to interior vertices
        for( M::MeshVertexIterator viter( m_pMesh ); !viter.end(); ++viter )
        {
            M::CVertex * pV = *viter;
            // Only update the interior vertex
            if( !pV->boundary() )
            {
                pV->huv()[k] = x( pV->idx() );
            }
        }
    }
}


//Compute the harmonic map with the boundary condition, iterative method
/*!	Iterative method compute harmonic map
*	\param epsilon error threshould
*/
template<typename M>
void CHarmonicMapper<M>::_iterative_map( double epsilon )
{
	//fix the boundary
	_set_boundary();

	//move interior each vertex to its center of neighbors
    for( M::MeshVertexIterator viter( m_pMesh ); !viter.end(); ++viter )
    {
        M::CVertex *pV = *viter;

        // We just process interior vertices.
        if( !pV->boundary() )
        {
            pV->huv() = CPoint2( 0, 0 );
        }
    }

    while( true )
    {
        double maxError = -1e+10;
        //move interior each vertex to its center of neighbors
        for( M::MeshVertexIterator viter( m_pMesh ); !viter.end(); ++viter )
        {
            M::CVertex *pV = *viter;

            // Only process interior vertices
            if( !pV->boundary() )
            {
                double  totalWeights = 0.0;
                CPoint2 totalWeightedHuv( 0, 0 ); // Default CPointn2 ctor is called.
                for( M::VertexVertexIterator vviter(pV); !vviter.end(); vviter ++ )
                {
                    M::CVertex *pNeighborVertex = *vviter;
                    M::CEdge *pE = m_pMesh->vertexEdge( pV, pNeighborVertex );

                    const double weight = pE->weight();
                    totalWeightedHuv += ( pNeighborVertex->huv() * weight );
                    totalWeights += weight;
                }

                const CPoint2 newUV( totalWeightedHuv / totalWeights );
                const double curError = ( pV->huv() - newUV ).norm();

                // Pick up the maximum error
                if( curError > maxError )
                {
                    maxError = curError;
                }

                // Update new uv.
                pV->huv() = newUV;
            }
        }

        std::cout<<"Current error: "<< maxError<<", epsilon: "<<epsilon<<std::endl;
        if( maxError < epsilon )
        {
            break;
        }
	}
}


};
#endif

